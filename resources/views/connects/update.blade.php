@extends('welcome')

@section('content')
<form action="{{url('/store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Name</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="name" required>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Slug</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="slug" required>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Adobe Magento to SAP</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="adobe_magento_to_sap" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="adobe_magento_to_sap" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Variant</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="variant" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="variant" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Category</label><br>
        <select class="form-select" aria-label="Default select example" name="category" required>
            <option selected>Select Category</option>
            <option value="eCommerce">eCommerce</option>
            <option value="ERP">ERP</option>
            <option value="PIM">PIM</option>
            <option value="CRM">CRM</option>
            <option value="Marketing">Marketing</option>
            <option value="Point of Sales">Point of Sales</option>
            <option value="EDI">EDI</option>
            <option value="OMS">OMS</option>
            <option value="IT">IT</option>
            <option value="Marketplaces">Marketplaces</option>
            <option value="Logistics">Logistics</option>
            <option value="AI">AI</option>
            <option value="Business Intelligence">Business Intelligence</option>
            <option value="Channel">Channel</option>
        </select>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Logo</label><br>
        <input type="file" id="myFile" name="image" onchange="loadFile(event)" accept="image/*" required><br>
        <img src="" alt="No image chosen" id="output" width="200px" height="200px" style="margin-top: 20px">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Logo Mini</label><br>
        <input type="file" id="myFile" name="image-2" onchange="loadMiniFile(event)" accept="image/*" required><br>
        <img src="" alt="No image chosen" id="output2" width="200px" height="200px" style="margin-top: 20px">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Native Connector</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="native" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="native" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Easy to Map</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="easy-to-map" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="easy-to-map" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Unique Intro Text</label>
        <textarea name="intro" class="form-control" aria-label="With textarea"></textarea>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Switch</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="switch" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="switch" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Test</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="test" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="test" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    {{-- <div class="mb-3 form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div> --}}
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
