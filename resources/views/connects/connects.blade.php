@extends('welcome')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{url('/create-connects')}}" class="btn btn-primary my-3">Add Connects</a>
        <button class="btn btn-danger my-3 delete">Delete Connects</button>
        <button class="btn btn-primary my-3 cancel" style="display: none">Cancel</button>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Responsive Hover Table</h3>
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right search"
                            placeholder="Search">

                        <div class="input-group-append">
                            <button type="submit" class="btn btn-default">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>System A</th>
                            <th>Category A</th>
                            <th>System B</th>
                            <th>Category B</th>
                            <th>Blogs Resources</th>
                            <th>Videos Resources</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $d)
                        <tr class="clickable-row" data-href="">
                            <td>
                                <form action="{{url('/delete-connects/'.$d->item_id)}}" method="post">
                                    @csrf
                                    @method('POST')
                                    <button class="btn btn-danger delete-connects" style="display: none">Delete</button>
                                </form>
                            </td>
                            <td class="connects-name">{{$d->name}}</td>
                            <td>{{$d->slug}}</td>
                            <td>{{$d->system_a}}</td>
                            <td>{{$d->category_a}}</td>
                            <td>{{$d->system_b}}</td>
                            <td>{{$d->category_b}}</td>
                            <td>{{$d->blogs_resources}}</td>
                            <td>{{$d->videos_resources}}</td>
                        </tr>
                        @endforeach
                        {{-- {{dd($data)}} --}}
                        {{$data->links()}}
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@endsection

@section('js')
<script>
    $('.delete').click(function () {
        $(this).css('display', 'none');
        $('.delete-connects').css('display', 'block');
        $('.cancel').css('display', 'inline-block');
    });
    $('.cancel').click(function () {
        $(this).css('display', 'none');
        $('.delete-connects').css('display', 'none');
        $('.delete').css('display', 'inline-block');
    })
    $('.search').on('keyup', function () {
        let value = $(this).val().toLowerCase();
        $('.connects-name').each(function () {
            $(this).filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
            if ($(this).text() != value) {
                $(this).css('display', 'none');
            }
        });
    })
</script>
@endsection
