@extends('welcome')

@section('content')
<form action="{{url('/post-connects')}}" method="POST">
    @csrf
    <select class="" name="platform" id="platforms">
        @foreach ($platforms as $item)
        <option value="{{$item->name}}">{{$item->name}}</option>
        @endforeach
    </select>
    <span> Connects to </span>
    <select class="" name="category" id="categories">
        @foreach ($categories as $c)
        <option value="{{$c->category}}">{{$c->category}}</option>
        @endforeach
    </select>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
