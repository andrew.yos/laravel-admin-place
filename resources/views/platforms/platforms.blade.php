@extends('welcome')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{url('/create')}}" class="btn btn-primary my-3">Add Platform</a>
        <button class="btn btn-danger my-3 delete">Delete Platform</button>
        <button class="btn btn-primary my-3 cancel" style="display: none">Cancel</button>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Responsive Hover Table</h3>
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input class="search-bar" type="text" name="table_search" class="form-control float-right"
                            placeholder="Search">

                        <div class="input-group-append">
                            <button type="submit" class="btn btn-default">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Category</th>
                            <th>Logo</th>
                            <th>Logo Mini</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $d)
                        <tr class="clickable-row" data-href="">
                            <td>
                                <form action="{{url('/delete/'.$d->item_id)}}" method="post">
                                @csrf
                                @method('POST')
                                <button class="btn btn-danger delete-platform" style="display: none">Delete</button>
                                </form>
                            </td>
                            <td><a class="marketplace-platform" href="{{url('/edit/'.$d->item_id)}}">{{$d->name}}</a>
                            </td>
                            <td>{{$d->slug}}</td>
                            <td>{{$d->category}}</td>
                            <td>{{$d->logo}}</td>
                            <td>{{$d->logo_mini}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@endsection

@section('js')
<script>
    $('.delete').click(function () {
        $(this).css('display', 'none');
        $('.delete-platform').css('display', 'block');
        $('.cancel').css('display', 'inline-block');
    });
    $('.cancel').click(function () {
        $(this).css('display', 'none');
        $('.delete-platform').css('display', 'none');
        $('.delete').css('display', 'inline-block');
    })
    $('.search-bar').on('keyup', function () {
        let value = $(this).val().toLowerCase();
        $('.marketplace-platform').each(function () {
            $(this).filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
            if ($(this).text() != value) {
                $(this).css('display', 'none');
            }
        });
    })

</script>
@endsection
