@extends('welcome')

@section('content')
<form action="{{url('/update/'.$data->item_id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Name</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="{{$data->name}}">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Slug</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="slug" value="{{$data->slug}}">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Featured</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="featured" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="featured" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Category</label>
        <select class="form-select" aria-label="Default select example" name="category">
            <option selected>{{$data->category}}</option>
            <option value="eCommerce">eCommerce</option>
            <option value="ERP">ERP</option>
            <option value="PIM">PIM</option>
            <option value="CRM">CRM</option>
            <option value="Marketing">Marketing</option>
            <option value="Point of Sales">Point of Sales</option>
            <option value="EDI">EDI</option>
            <option value="OMS">OMS</option>
            <option value="IT">IT</option>
            <option value="Marketplaces">Marketplaces</option>
            <option value="Logistics">Logistics</option>
            <option value="AI">AI</option>
            <option value="Business Intelligence">Business Intelligence</option>
            <option value="Channel">Channel</option>
        </select>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Logo</label><br>
        <input type="file" id="myFile" name="image" onchange="loadFile(event)" accept="image/*" ><br>
        <img src="{{$data->logo}}" alt="No image chosen" id="output" width="200px" height="200px" style="margin-top: 20px">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Logo Mini</label><br>
        <input type="file" id="myFile" name="image-2" onchange="loadMiniFile(event)" accept="image/*" ><br>
        <img src="{{$data->logo_mini}}" alt="No image chosen" id="output2" width="200px" height="200px" style="margin-top: 20px">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Native Connector</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="native" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="native" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Easy to Map</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="easy-to-map" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="easy-to-map" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Unique Intro Text</label>
        <textarea name="intro" class="form-control" aria-label="With textarea">{{$data->unique_intro_text}}</textarea>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Switch</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="switch" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="switch" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Test</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="test" id="flexRadioDefault1" value="1">
            <label class="form-check-label" for="flexRadioDefault1">
                On
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="test" id="flexRadioDefault2" value="0" checked>
            <label class="form-check-label" for="flexRadioDefault2">
                Off
            </label>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection

@section('js')
<script>
    var loadFile = function (event) {
        var image = document.getElementById('output');
        image.src = URL.createObjectURL(event.target.files[0]);
    };
    var loadMiniFile = function (event) {
        var image = document.getElementById('output2');
        image.src = URL.createObjectURL(event.target.files[0]);
    };
</script>
@endsection
