<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'App\Http\Controllers\UsersController@index');
// Route::post('/login', 'App\Http\Controllers\UsersController@auth');


Route::get('/', 'App\Http\Controllers\PlatformsController@index');
Route::get('/create','App\Http\Controllers\PlatformsController@create');
Route::post('/store', 'App\Http\Controllers\PlatformsController@store');
Route::get('/edit/{id}', 'App\Http\Controllers\PlatformsController@edit');
Route::put('/update/{id}', 'App\Http\Controllers\PlatformsController@update');
Route::post('/delete/{id}', 'App\Http\Controllers\PlatformsController@destroy');


Route::get('/connects', 'App\Http\Controllers\ConnectsController@index');
Route::get('/create-connects', 'App\Http\Controllers\ConnectsController@create');
Route::post('/post-connects', 'App\Http\Controllers\ConnectsController@post');
Route::post('/delete-connects/{id}', 'App\Http\Controllers\ConnectsController@destroy');
