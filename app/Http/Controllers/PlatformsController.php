<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Platforms;
use App\Models\Category;
use Illuminate\Support\Str;
use CloudinaryLabs\CloudinaryLaravel\CloudinaryEngine;

class PlatformsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Platforms::all();

        return view('platforms.platforms',[
            'data' => $datas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Platforms;

        return view('platforms.create-platform', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $platforms = new Platforms;
        $randomString = Str::random(24);
        $request->file('image')->store('platform-logo', 'public');
        $request->file('image-2')->store('platform-img', 'public');
        $image = cloudinary()->upload($request->file('image')->getRealPath())->getSecurePath();
        $miniImage = cloudinary()->upload($request->file('image-2')->getRealPath())->getSecurePath();

        $platforms->name = $request->input('name');
        $platforms->slug = $request->input('slug');
        $platforms->item_id = $randomString;
        $platforms->featured = $request->input('featured');
        $platforms->category = $request->input('category');
        $platforms->logo = $image;
        $platforms->logo_mini = $miniImage;
        $platforms->native_connector = $request->input('native');
        $platforms->easy_to_map = $request->input('easy-to-map');
        $platforms->unique_intro_text = $request->input('intro');
        $platforms->switch = $request->input('switch');
        $platforms->test = $request->input('test');

        $platforms->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $platforms = Platforms::where('item_id', $id)->first();
        $categories = Category::all();

        return view('platforms.update-platform', [
            'data' => $platforms,
            'category' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $platforms = Platforms::find($id);
        $platforms_Id = Platforms::where('item_id', $id)->first();
        $logo = $platforms_Id->logo;
        $logo_mini = $platforms_Id->logo_mini;
        
        $platforms->name = $request->input('name');
        $platforms->slug = $request->input('slug');
        $platforms->item_id = $id;
        $platforms->featured = $request->input('featured');
        $platforms->category = $request->input('category');
        if ($request->hasFile('image')) {
            $platforms->logo = cloudinary()->upload($request->file('image')->getRealPath())->getSecurePath();
            $platforms->logo_mini = $logo_mini;
        }
        elseif ($request->hasFile('image-2')) {
            $platforms->logo = $logo;
            $platforms->logo_mini = cloudinary()->upload($request->file('image-2')->getRealPath())->getSecurePath();
        }
        elseif ($request->hasFile('image') && $request->hasFile('image-2')) {
            $platforms->logo = cloudinary()->upload($request->file('image')->getRealPath())->getSecurePath();
            $platforms->logo_mini = cloudinary()->upload($request->file('image-2')->getRealPath())->getSecurePath();
        }
        else {
            $platforms->logo = $logo;
            $platforms->logo_mini = $logo_mini;
        }  
        $platforms->native_connector = $request->input('native');
        $platforms->easy_to_map = $request->input('easy-to-map');
        $platforms->unique_intro_text = $request->input('intro');
        $platforms->switch = $request->input('switch');
        $platforms->test = $request->input('test');

        $platforms->update();

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Platforms $platforms, $id)
    {
        $platforms = Platforms::find($id);
		$platforms->delete();
		return redirect('/');
    }
}
