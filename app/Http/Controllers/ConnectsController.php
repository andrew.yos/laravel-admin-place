<?php

namespace App\Http\Controllers;

use App\Models\Connects;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Platforms;
use App\Models\Category;

class ConnectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Connects::simplePaginate(100);

        return view('connects.connects',[
            'data' => $datas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $platforms = Platforms::all();
        $categories = Category::all();



        return view('connects.create',[
            'categories' => $categories,
            'platforms' => $platforms
        ]);
    }

    public function post(Request $request)
    {
        $platforms = $request->input('platform');
        $categories = $request->input('category');
        $randomString = Str::random(24);
        $videos_slug = ['alumios-sap-connector-for-adobe-magento',
        'b2b-modules-magento',
        'how-alumio-provides-flexibility-in-data-integrations',
        'how-to-integrate-magento-with-sap',
        'how-to-integrate-shopware-with-sap',
        'how-to-integrate-software-with-sap',
        'integrate-your-erp-with-ecommerce-and-no-limitations',
        'integrating-pimcore-using-alumio-ipaas',
        'real-demo-ecommerce-integration-with-dynamics',
        'why-is-our-digital-to-sap-integration-unique',
        'why-is-our-dynamics-integration-to-digital-applications-unique',
        'why-is-our-magento-sap-integration-unique',
        'why-is-our-shopware-sap-integration-unique',
        'why-is-our-magento-microsoft-dynamics-integration-unique',
        'how-to-integrate-magento-with-microsoft-dynamics',
        'why-is-our-shopware-microsoft-dynamics-integration-unique',
        'how-to-integrate-shopware-with-microsoft-dynamics',
        'connect-akeneo-to-channelengine',
        'connect-shopware-to-akeneo',
        'connect-microsoft-dynamics-365-finance-and-supply-chain-management-to-magento',
        'is-your-sap-not-able-to-deliver-the-right-data-as-webservices',
        'microsoft-dynamics-business-central-api-plugin',
        '3-key-reasons-for-choosing-the-sap-api-plugin',
        'how-to-connect-your-software-in-3-easy-steps',
        'experience-ipaas-webinar-beginners',
        'on-demand-webinar-experience-ipaas-experts',
        'integration-platform-for-commerce-connectivity',
        'accelerate-your-time-to-market-and-boost-digitalization-with-the-ipaas',
        'be-in-control-of-your-data-and-integrations',
        'connect-your-relations-via-edi-or-standard-data-formats',
        'data-access-business-intelligence-artificial-intelligence-and-machine-learning',
        'general-problems-you-might-be-facing',
        'infrastructural-insurance-and-integrational-inconveniences',
        'know-your-customers-grow-your-business',
        'privacy-regulations-and-compliance',
        'reduce-the-cost-of-your-data-and-integrations',
        'simplicity-is-genius-lower-threshold-for-technical-competence'];

        $blog_slugs = ['40-tips-to-integrate-erp-to-ecommerce',
        'alumio-and-cloudsuite-integration',
        'alumio-and-shopctrl-integration',
        'alumio-is-technology-partner-of-marello',
        'choosing-an-integration-strategy-for-shopware-6',
        'ecommerce-to-sap-and-dynamics',
        'magento-and-centric-erp-integration',
        'magento-orders-to-sap-erp-central-component-ecc',
        'yotpo-connector-template',
        'stamped-io-connector-template',
        'sap-api-plugin-and-ipaas',
        'sap-api-plugin-benefits',
        'sap-api-plugin-financial-business-case',
        'dynamics-api-plugin-and-ipaas',
        'dynamics-api-plugin-benefits',
        'dynamics-api-plugin-financial-business-case',
        'dynamics-fo-api-plugin-and-ipaas',
        'dynamics-fo-api-plugin-benefits',
        'dynamics-fo-api-plugin-financial-business-case',
        'ipaas-strategic-digital-pillar',
        'ipaas-benefits',
        'ipaas-compliant-and-secure',
        'ipaas-growth-and-scalability',
        'ipaas-scalable-it-landscape',
        'custom-code-integrations',
        'enable-connectivity-with-ipaas',
        'ipaas-360-view-of-customers',
        'ipaas-agile',
        'middleware-integrations',
        'plugin-integrations'];
        
        

        $getSystemA = Platforms::where('name', $platforms)->first();
        $getSystemB = Platforms::where('category', $categories)->get();

        foreach ($getSystemB as $b => $value) {
            $connects = new Connects;
            $nameA = $getSystemA->name;
            $nameB = $value->name;
            $slugA = $getSystemA->slug;
            $slugB = $value->slug;
            $categoryA = $getSystemA->category;
            $categoryB = $value->category;
            $random_videos = array_rand($videos_slug, 3);
            $resourceVideos = [];
            foreach ($random_videos as $r => $value) {
                array_push($resourceVideos, $videos_slug[$value]);
            }
            $videos = json_encode($resourceVideos);
            // dd($resourceVideos);
            $random_blog = array_rand($blog_slugs, 3);
            $resourceBlog = [];
            foreach ($random_blog as $r => $value) {
                array_push($resourceBlog, $blog_slugs[$value]);
            }
            $blogs = json_encode($resourceBlog);

            $integrationFamily = '';

            if (str_contains($nameA, 'Microsoft Dynamics') || str_contains($nameB, 'Microsoft Dynamics')) {
                $integrationFamily = 'microsoft-dynamics';
            }
            elseif (str_contains($nameA, 'SAP') || str_contains($nameB, 'SAP')) {
                $integrationFamily = 'sap';
            }
            elseif (str_contains($nameA, 'Sage') || str_contains($nameB, 'Sage')) {
                $integrationFamily = 'sage';
            }

            $connects->name = $nameA.' to '.$nameB;
            $connects->slug = $slugA.'-to-'.$slugB;
            $connects->item_id = $randomString;
            $connects->adobe_magento_to_sap = 0;
            $connects->integration_family = $integrationFamily;
            $connects->system_a = $slugA;
            $connects->category_a = $categoryA;
            $connects->text_a = $slugA;
            $connects->system_b = $slugB;
            $connects->category_b = $categoryB;
            $connects->text_b = $slugB;
            $connects->dynamic_group_words = null;
            $connects->blogs_resources = $blogs;
            $connects->videos_resources = $videos;
            $connects->save();
        }
        return redirect('/connects');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Connects  $connects
     * @return \Illuminate\Http\Response
     */
    public function show(Connects $connects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Connects  $connects
     * @return \Illuminate\Http\Response
     */
    public function edit(Connects $connects)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Connects  $connects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Connects $connects)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Connects  $connects
     * @return \Illuminate\Http\Response
     */
    public function destroy(Connects $connects, $id)
    {
        $connects = Connects::find($id);
		$connects->delete();
		return redirect('/connects');
    }
}
