<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Connects extends Model
{
    protected $table = 'connects';
    protected $primaryKey = 'item_id';
    public $timestamps = false;

    protected $keyType = 'string';
}
