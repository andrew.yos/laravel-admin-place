<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Platforms extends Model
{
    protected $table = 'platforms';
    protected $primaryKey = 'item_id';
    public $timestamps = false;

    protected $keyType = 'string';

}
